using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System;
using UniRx;

namespace EggClicker.Facility
{
    [RequireComponent(typeof(Button))]
    public abstract class FacilityBase
    {
        /// 抽象プロパティ
        /// 子で実装させたい場合に利用する 

        [SerializeField]
        protected string m_facilityName;
        [SerializeField]
        protected double m_currentPossession;
        [SerializeField]
        protected double m_basePrice;
        [SerializeField]
        protected double m_baseEps;
        [SerializeField, ReadOnly]
        protected double m_currentPrice;

        /// <summary>
        /// 施設の名前
        /// </summary>
        public virtual string FacilityName 
        {
            get { return m_facilityName;}
            protected set { value = m_facilityName; }
        }

        /// <summary>
        /// 施設の初期生産量
        /// </summary>
        public virtual double BaseEpS { get; protected set; }

        /// <summary>
        /// 施設の基礎価格
        /// </summary>
        public virtual double BasePrice { get { return m_basePrice; } protected set { value = m_basePrice; } }


        // 多分ここらへんプロパティで設定できる型指定されたRP使ったほうが楽そう

        /// <summary>
        /// 施設の現在価格のプロパティ
        /// </summary>
        protected virtual ReactiveProperty<double> CurrentPriceRP { get; set; }

        /// <summary>
        /// 施設の現在価格の公開プロパティ
        /// </summary>
        [ReadOnly]
        public virtual IReadOnlyReactiveProperty<double> CurrentPriceRORP { get; protected set; }

        /// <summary>
        /// 施設の現在所持数の公開プロパティ
        /// </summary>
        protected virtual ReactiveProperty<double> CurrentPossessionRP { get; set; }

        /// <summary>
        /// 施設の現在所持数の公開プロパティ
        /// </summary>
        public virtual IReadOnlyReactiveProperty<double> CurrentPossessionRORP { get; protected set; }

        /// <summary>
        /// 施設の画像
        /// </summary>
        public virtual Texture2D Texture { get; protected set; }

        /// <summary>
        /// 値の初期化
        /// </summary>
        public virtual void InitValue()
        {
            if (m_currentPossession == 0)
            {
                m_currentPrice = m_basePrice;
            }

            // RPの初期化
            CurrentPriceRP = new ReactiveProperty<double>(m_currentPrice);
            CurrentPriceRORP = CurrentPriceRP;

            CurrentPossessionRP = new ReactiveProperty<double>(m_currentPossession);
            CurrentPossessionRORP = CurrentPossessionRP;

            BaseEpS = m_baseEps;
        }


        private const double m_reCulcPriceMultipuler = 1.15;

        public void ReculcPrice() 
        {
            CurrentPriceRP.Value = Math.Ceiling(BasePrice * Math.Pow(m_reCulcPriceMultipuler, CurrentPossessionRP.Value));
        }

        public void Buy(int amount = 1)
        {
            CurrentPossessionRP.Value += amount;
            ReculcPrice();
        }
    }


}
