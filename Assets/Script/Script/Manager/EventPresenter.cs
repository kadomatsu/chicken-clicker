using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using EggClicker.UI;
using EggClicker.Manager;

using UnityEngine.UI;
using System;

namespace EggClicker.Manager
{
    public class EventPresenter : MonoBehaviour
    {
        HenView m_HenView;
        EpSView m_EpsView;
        GameManager m_GameManager;
        FacilityManager m_FacilityManager;
        [SerializeField]
        Button m_hiyokoButton;
        [SerializeField]
        Button m_niwatoriButton;
        [SerializeField]
        Button m_ukokkeiButton;
        [SerializeField]
        Button m_datyoButton;
        [SerializeField]
        Button m_sisotyo;
        [SerializeField]
        Button m_phoenix;

        void Start()
        {
            // ゲームマネージャーの取得
            // すべてのObserverに.AddTo(GameManager)するため、購読の前に処理を行う
            m_GameManager = GameObject.Find("Managers").GetComponent<GameManager>();
            m_FacilityManager = this.GetComponent<FacilityManager>();
            
            // View --> Model
            SubscribeHenClickEvent();

            // Model --> View
            SubscribeChangeEpS();
            SubscribeChangeEgg();
        }


        void SubscribeHenClickEvent()
        {
            m_HenView = GameObject.Find("Hen").GetComponent<HenView>();
            m_HenView.m_clickObservable
                .Subscribe(
                    _ => m_GameManager.EarnEgg(1) // TODO ここクリック時の増加分をマジックナンバーでかいている
                )
                .AddTo(m_GameManager);
        }

        /// <summary>
        /// 現在の生産量が変更されたときの通知を受け取り、Viewのメソッドを実行する
        /// GM => Viewのデータ変更通知を行う
        /// </summary>
        void SubscribeChangeEpS()
        {
            m_EpsView = GameObject.Find("EpS").GetComponent<EpSView>();
            m_GameManager.m_currentEpSObservable
                .Subscribe(
                    x => m_EpsView.UpdateTxt(x)
                ).AddTo(m_GameManager);
        }

        void SubscribeChangeEgg()
        {
            var hiyoko = m_FacilityManager.Hiyoko;
            // ひよこ
            // 購入可能なら、ボタンをアクティブにする
            m_GameManager.m_currentEggObservable
                .Select(x => x >= m_FacilityManager.Hiyoko.CurrentPriceRORP.Value)
                .DistinctUntilChanged()
                .Subscribe(
                    x => m_hiyokoButton.interactable = x
                );

            // にわとり
            var niwatori = m_FacilityManager.Niwatori;
            m_GameManager.m_currentEggObservable
                .Select(x => x >= m_FacilityManager.Niwatori.CurrentPriceRORP.Value)
                .DistinctUntilChanged()
                .Subscribe(
                    x => m_niwatoriButton.interactable = x
                );

            var ukokkei = m_FacilityManager.Ukokkei;
            m_GameManager.m_currentEggObservable
                .Select(x => x >= m_FacilityManager.Ukokkei.CurrentPriceRORP.Value)
                .DistinctUntilChanged()
                .Subscribe(
                    x => m_ukokkeiButton.interactable = x
                );

            var datyo = m_FacilityManager.Datyo;
            m_GameManager.m_currentEggObservable
                .Select(x => x >= m_FacilityManager.Datyo.CurrentPriceRORP.Value)
                .DistinctUntilChanged()
                .Subscribe(
                    x => m_datyoButton.interactable = x
                );

            var sisotyo = m_FacilityManager.Sisotyo;
            m_GameManager.m_currentEggObservable
                .Select(x => x >= m_FacilityManager.Sisotyo.CurrentPriceRORP.Value)
                .DistinctUntilChanged()
                .Subscribe(
                    x => m_sisotyo.interactable = x
                );

            var phoenix = m_FacilityManager.Phoenix;
            m_GameManager.m_currentEggObservable
                .Select(x => x >= m_FacilityManager.Phoenix.CurrentPriceRORP.Value)
                .DistinctUntilChanged()
                .Subscribe(
                    x => m_phoenix.interactable = x
                );

        }
    }
}

