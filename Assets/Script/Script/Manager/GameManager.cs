using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UniRx;
using EggClicker.Facility;

namespace EggClicker.Manager
{
    public class GameManager : SingletonMonoBehaviour<GameManager>
    {
        /// <summary>
        /// 現在Eggの公開オブザーバブル
        /// </summary>
        public IReadOnlyReactiveProperty<double> m_currentEggObservable
        {
            get { return m_currentEgg; }
        }

        // 現在のEGG所持数のRP
        [SerializeField]
        public DoubleReactiveProperty m_currentEgg;

        /// <summary>
        /// 現在のEGG所持数の値を返す公開プロパティ
        /// </summary>
        public double CurrentEggValue
        {
            get { return m_currentEgg.Value; }
            private set { m_currentEgg.Value = value; }
        }

        private ReactiveProperty<double> m_currentEpSRP;

        /// <summary>
        /// 現在生産量の公開オブザーバブル
        /// </summary>
        public IReadOnlyReactiveProperty<double> m_currentEpSObservable
        {
            get { return m_currentEpSRP; }
        }

        /// <summary>
        /// 現在生産量の値を返す公開プロパティ
        /// </summary>
        public double m_currentEpSValue
        {
            get { return m_currentEpSRP.Value; }
            private set { m_currentEpSRP.Value = value; }
        }

        override protected void Awake()
        {
            base.Awake();
            // RPの初期化
            m_currentEpSRP = new ReactiveProperty<double>(0);
            m_currentEgg = new DoubleReactiveProperty(0);
        }

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            EarnEgg(m_currentEpSValue * Time.deltaTime);
        }

        public void EarnEgg(double howmany)
        {
            m_currentEgg.Value += howmany;
        }

        public void SpendEgg(double howmany)
        {
            m_currentEgg.Value -= howmany;
        }

        public void IncrimentEpS(double howmany)
        {
            m_currentEpSRP.Value += howmany;
        }

    }
}
