using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EggClicker.Facility;
using Sirenix.OdinInspector;
using System;

namespace EggClicker.Manager
{
    public class FacilityManager : SerializedMonoBehaviour
    {
        private static FacilityManager m_instance;
        public static FacilityManager Instance
        {
            get
            {
                if (m_instance == null)
                {
                    m_instance = (FacilityManager)FindObjectOfType(typeof(FacilityManager));
                    if (m_instance == null)
                    {
                        Debug.LogError(typeof(FacilityManager) + "アタッチしているGameObject");
                    }
                }
                return m_instance;
            }
        }

        // ボタンの初期化
        [SerializeField]
        FacilityBase m_hiyoko;
        public FacilityBase Hiyoko
        { 
            get { return m_hiyoko; }
            private set { Hiyoko = m_hiyoko; }
        }

        [SerializeField]
        FacilityBase m_niwatori;
        public FacilityBase Niwatori
        {
            get { return m_niwatori; }
            private set { Niwatori = m_niwatori; }
        }

        [SerializeField]
        FacilityBase m_ukokkei;
        public FacilityBase Ukokkei
        {
            get { return m_ukokkei; }
            private set { Ukokkei = m_ukokkei; }
        }

        [SerializeField]
        FacilityBase m_datyo;
        public FacilityBase Datyo
        {
            get { return m_datyo; }
            private set { Datyo = m_datyo; }
        }

        [SerializeField]
        FacilityBase m_sisotyo;
        public FacilityBase Sisotyo
        {
            get { return m_sisotyo; }
            private set { Sisotyo = m_sisotyo; }
        }

        [SerializeField]
        FacilityBase m_phoenix;
        public FacilityBase Phoenix
        {
            get { return m_phoenix; }
            private set { Phoenix = m_phoenix; }
        }


        void Awake()
        {
            if (m_instance == null)
            {
                m_instance = this;
            }
            else Destroy(this);

            instanciateFacilities();

        }


        /// <summary>
        /// 施設の購入メソッド
        /// </summary>
        /// <param name="facility"></param>
        public void PurchaseFacility(FacilityBase facility)
        {
            try
            {
                // 購入してから、施設側を更新する
                GameManager.Instance.SpendEgg(facility.CurrentPriceRORP.Value);
                facility.Buy();
                GameManager.Instance.IncrimentEpS(facility.BaseEpS);
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

        private void instanciateFacilities()
        {
            m_hiyoko.InitValue();
            m_niwatori.InitValue();
            m_ukokkei.InitValue();
            m_datyo.InitValue();
            m_sisotyo.InitValue();
            m_phoenix.InitValue();
        }
    }
}
