using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;
using EggClicker.UI;
using EggClicker.Facility;

namespace EggClicker.Manager
{
    public class FacilityPresenter : MonoBehaviour
    {
        // viewのボタンを取得
        [SerializeField]
        Button m_button1;
        [SerializeField]
        Button m_button2;
        [SerializeField]
        Button m_button3;
        [SerializeField]
        Button m_button4;
        [SerializeField]
        Button m_button5;
        [SerializeField]
        Button m_button6;

        FacilityButtonView m_facility1;
        FacilityButtonView m_facility2;
        FacilityButtonView m_facility3;
        FacilityButtonView m_facility4;
        FacilityButtonView m_facility5;
        FacilityButtonView m_facility6;

        // Start is called before the first frame update
        void Start()
        {

            var hiyoko = FacilityManager.Instance.Hiyoko;
            m_facility1 = m_button1.GetComponent<FacilityButtonView>();

            var niwatori = FacilityManager.Instance.Niwatori;
            m_facility2 = m_button2.GetComponent<FacilityButtonView>();

            var ukokkei = FacilityManager.Instance.Ukokkei;
            m_facility3 = m_button3.GetComponent<FacilityButtonView>();

            var datyo = FacilityManager.Instance.Datyo;
            m_facility4 = m_button4.GetComponent<FacilityButtonView>();

            var sisotyo = FacilityManager.Instance.Sisotyo;
            m_facility5 = m_button5.GetComponent<FacilityButtonView>();

            var phoenix = FacilityManager.Instance.Phoenix;
            m_facility6 = m_button6.GetComponent<FacilityButtonView>();


            // Model -> Viewへの通知を購読する
            StartSubscribes(m_facility1, hiyoko);
            StartSubscribes(m_facility2, niwatori);
            StartSubscribes(m_facility3, ukokkei);
            StartSubscribes(m_facility4, datyo);
            StartSubscribes(m_facility5, sisotyo);
            StartSubscribes(m_facility6, phoenix);


            // ボタンの表示を初期化する
            InitButtonText(m_facility1, hiyoko);
            InitButtonText(m_facility2, niwatori);
            InitButtonText(m_facility3, ukokkei);
            InitButtonText(m_facility4, datyo);
            InitButtonText(m_facility5, sisotyo);
            InitButtonText(m_facility6, phoenix);
        }

        private void InitButtonText(FacilityButtonView facilityBtnView , FacilityBase facility)
        {
            // Buttonのテキストに値を設定する
            facilityBtnView.setPossessText(facility.CurrentPossessionRORP.Value);
            facilityBtnView.setFacilityName(facility.FacilityName);
            facilityBtnView.setPriceText(facility.CurrentPriceRORP.Value);
            facilityBtnView.setEpSText(facility.BaseEpS);
        }

        private void StartSubscribes(FacilityButtonView facilityBtnView, FacilityBase facility)
        {
            // View -> Modelへの通知を購読
            facilityBtnView.OnClickObservable
            .Subscribe(x => FacilityManager.Instance.PurchaseFacility(facility))
            .AddTo(this);

            // Model -> Viewの通知を購読する
            // 施設の価格
            facility.CurrentPriceRORP
                .Subscribe(x => facilityBtnView.setPriceText(x));

            // 施設の所持数
            facility.CurrentPossessionRORP
                .Subscribe(x => facilityBtnView.setPossessText(x));

        }
    }
}


