using System.Collections;
using System;
using UnityEngine;
using EggClicker.Facility;
using UnityEngine.UI;
using UniRx;
using Sirenix.OdinInspector;

namespace EggClicker.UI
{
    [RequireComponent(typeof(Button))]

    public class FacilityButtonView : SerializedMonoBehaviour
    {
        private GameObject m_txetObj;
        private Text m_textComp;
        private Button m_button;
        private Subject<Unit> m_onClickSubject;
        private Text m_possessText;
        
        [SerializeField, ShowInInspector]
        public Text PossessText { get; private set; }
        
        [SerializeField, ShowInInspector]
        public Text PriceText { get; private set; }
        
        [SerializeField]
        public Text FacilityNameText { get; private set; }

        [SerializeField]
        public Text FacilityEpS { get; private set; }

        /// <summary>
        /// ボタンクリック通知用公開オブザーバブル
        /// </summary>
        public IObservable<Unit> OnClickObservable { get; private set; }


        private void Awake()
        {
            // ボタンの取得
            m_button = this.gameObject.GetComponent<Button>() as Button;

            // テキストの取得
            var priceText = PriceText.GetComponent<Text>();
            m_possessText = PossessText.GetComponent<Text>();

            // ボタンのクリックイベントを初期化する
            OnClickObservable = m_button.OnClickAsObservable();
        }

        // Start is called before the first frame update
        void Start()
        {

        }
        
        public void setFacilityName(String facilityName)
        {
            // 子オブジェクトのTextに施設名を設定する
            FacilityNameText.text = facilityName;
        }

        public void setPossessText(double input)
        {
            // PossessTextのセッターに仕込んでもいいが、気持ち悪いのでメソッド化する
            m_possessText.text = NumberFormater.FormatDouble(input);
        }
        public void setPriceText(double input)
        {
            PriceText.text = "€ " + NumberFormater.FormatDouble(input);
        }

        public void setEpSText(double input)
        {
            FacilityEpS.text = "+ " + NumberFormater.EpSFormat(input) + " EpS";
        }
    }
}

