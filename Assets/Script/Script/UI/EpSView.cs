using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace EggClicker.UI
{
    /// <summary>
    /// シーン上のEpS Objectにアタッチされる前提のクラス
    /// EpSの表示をコントロールする
    /// </summary>
    public class EpSView : MonoBehaviour
    {
        private Text m_text;

        void Awake()
        {
            m_text = this.GetComponent<Text>();
        }

        /// <summary>
        /// テキストを更新する
        /// </summary>
        /// <param name="input"></param>
        public void UpdateTxt(double input)
        {

            m_text.text = "Egg毎秒 (EpS): " + NumberFormater.EpSFormat(input);
        }
    }
}

