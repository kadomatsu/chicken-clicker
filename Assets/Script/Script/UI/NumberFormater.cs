using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace EggClicker.UI
{
    public static class NumberFormater
    {
        /// <summary>
        /// 数値を整数値に切り捨てして、1000区切りの","を追加する
        /// </summary>
        public static string FormatDouble(double input)
        {
            return Math.Floor(input).ToString("#,0");
        }

        /// <summary>
        /// EpS表示を意図した、文字列フォーマッタ
        /// 999,999.9形式を返す
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string EpSFormat(double input)
        {
            // 999,999.9の形式で表示するため桁をシフトする
            var RoundedNum = Math.Floor(input * 10) / 10;
            return RoundedNum.ToString("#,0.#");
        }
    }
}

