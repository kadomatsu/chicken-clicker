using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EggClicker.Manager;
using UnityEngine.UI;
using System;

namespace EggClicker.UI
{
    /// <summary>
    /// 現在所持しているEggを表示するView
    /// </summary>
    [RequireComponent(typeof(Text))]
    public class EggView : MonoBehaviour
    {
        GameManager m_gameManager;
        Text m_text;

        void Start()
        {
            m_gameManager = GameObject.Find("Managers").GetComponent<GameManager>();
            m_text = this.GetComponent<Text>();
        }

        // Update is called once per frame
        void Update()
        {
            var rawCurrentEggNumber = this.m_gameManager.m_currentEggObservable.Value;

            // 数値を切り捨てして、1000区切りの","を追加
            // 頭に"€"記号を追加 
            m_text.text = "€ " + NumberFormater.FormatDouble(rawCurrentEggNumber) + " (Eggs)";
        }
    }
}