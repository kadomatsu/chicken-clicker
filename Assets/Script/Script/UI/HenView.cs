using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UniRx;


namespace EggClicker.UI
{
    public class HenView : MonoBehaviour, IPointerClickHandler

    {
        private Subject<Unit> m_clickSubject;

        /// <summary>
        /// クリック検知用の公開Observable
        /// </summary>
        public IObservable<Unit> m_clickObservable { get; private set; }

        public void Awake()
        {
            m_clickSubject = new Subject<Unit>();
            m_clickObservable = m_clickSubject.AsObservable();
        }

        /// <summary>
        /// クリックされたとき、購読者にイベントを通知する
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerClick(PointerEventData eventData)
        {
            // 左クリックのみ検知する
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                m_clickSubject.OnNext(Unit.Default);
            }
        }
    }
}